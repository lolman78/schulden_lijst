@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row mb-4">
            <div class="col-md-3 p-0">
                <div class="card">
                    <div class="card-header">
                        Admin menu
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.users') }}" role="tab"
                           aria-controls="v-pills-team" aria-selected="false">Gebruikers</a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.debts') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Schulden</a>
                        <a class="nav-link" id="v-pills-user-tab" href="{{ route('admin.roles') }}" role="tab"
                           aria-controls="v-pills-user" aria-selected="false">Rollen</a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.statuses') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="true"><b>Statuses</b></a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h1 style="display: inline-block;">Statuses</h1>

                        <div class="dropdown" style="display: inline-block; float: right;">
                            <button class="btn btn-secondary  float-right" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Acties
                            </button>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item btn float-right"
                                    href="{{ route('user.create') }}">{{ ('Nieuwe Gebruiker maken') }}
                                </a>
                                <a class="dropdown-item btn float-right"
                                   href="{{ route('debt.create') }}">{{ ('Nieuwe Schuld maken') }}
                                </a>
                                <a class="dropdown-item btn float-right"
                                   href="{{ route('role.create') }}">{{ ('Nieuwe Rol maken') }}
                                </a>
                                <a class="dropdown-item btn float-right"
                                   href="{{ route('status.create') }}">{{ ('Nieuwe Status Maken') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <table class="rwd-table">
                        <tbody>
                            <tr>
                                <th style="padding-left: 10px; padding-top: 10px">ID</th>
                                <th style="padding-left: 10px; padding-top: 10px">Status</th>
                                <th style="padding-top: 10px">Created_at</th>
                                <th style="padding-top: 10px">Updated_at</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach($statuses as $status)
                                <tr>
                                    <td data-th="id" style="padding-left: 10px">
                                        {{ $status->id }}
                                    </td>
                                    <td data-th="Name" style="padding-left: 10px">
                                        {{ $status->name }}
                                    </td>
                                    <td data-th="Created_at">
                                        @if($status->created_at)
                                            {{ date_format( $status->created_at, 'd-m-Y') }}
                                        @endif
                                    </td>
                                    <td data-th="Updated_at">
                                        @if($status->updated_at)
                                            {{ date_format( $status->updated_at, 'd-m-Y') }}
                                        @endif
                                    </td>
                                    <td data-th="Acties">
                                        <form method="POST" action="{{ route('status.destroy', $status) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit"
                                                onclick="return confirm('weet je zeker dat je de status {{$status->name}} wilt verwijderen?')"
                                                class="btn text-secondary">Verwijderen
                                            </button>
                                        </form>
                                    </td>
                                    <td data-th="Acties">
                                        <a href="{{ route('status.edit', $status) }}"
                                           class="btn btn-primary">Bewerken
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
