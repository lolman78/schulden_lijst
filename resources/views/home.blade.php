@extends('layouts.app')

@section('content')
<div class="container">
    @if(!Auth::User() == null)
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        Totaal ontvangen geld:  €{{ $recievedMoney }}<br>    
                        
                        Totaal geld dat je nog moet krijgen: €{{ $unrecievedMoney }}<br>

                        Aantal betaalde schulden: {{ $paidDebts }}<br>

                        Aantal onbetaalde Schulden: {{ $unpaidDebts }}<br>

                        Totaal aantal schulden: {{ $numberOfDebts }}<br>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="text-center">
                    <p>
                        <h1>Welkom {{ $user->name }}</h1>

                        <p>
                            wilt u een nieuwe schuld maken?<br>
                            <a href="{{ route('debt.create') }}"
                                class="btn btn-primary">Schuld maken
                            </a>
                        </p>

                        <p>
                            Wilt u uw schulden bekijken?<br>
                            <a href="{{ route('user.myDebts', $user) }}"
                                class="btn btn-primary">Schulden bekijken
                            </a>
                        </p>
                    </p>
                </div>
            </div>
        </div>
    @endIf
</div>
@endsection
