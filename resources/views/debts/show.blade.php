@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="margin-bottom: 20px">
                    <div class="card-header"></div>
                    <div class="card-body">
                        <h1>Schuld voor: {{ $debt->debt_for }}</h1>
                        <hr>
                        <span><b>Prijs:</b> €{{ $debt->price }}</span><br>
                        <span><b>Status:</b> {{ $debt->status->name }}</span>
                        
                        <p>{!! $debt->description !!}</p>
                    </div>

                    <div class="row">
                        <p style="margin-left: 20px;">
                            @can('editAndUpdate', $debt)
                                <div class="col-md-2" style="margin-bottom: 20px;">
                                    <a href="{{ route('debt.edit', $debt) }}"
                                    class="btn btn-primary">Bewerken
                                    </a>
                                </div>
                            @endcan

                            @can('destroy', $debt)
                                <div class="col-md-2 ">
                                    <form method="POST"
                                        action="{{ route('debt.destroy', $debt) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                            onclick="return confirm('weet je zeker dat je deze schuld wilt verwijderen?')"
                                            class="btn text-secondary">Verwijderen
                                        </button>
                                    </form>
                                </div>
                            @endcan

                            <div class="col-md-2" style="margin-top: 7px">
                                <a class="text-secondary"
                                    href="{{ route('user.myDebts', $debt->user) }}">Terug
                                </a>
                            </div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
