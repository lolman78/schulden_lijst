@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Schuld bewerken') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('debt.update', $debt )}}" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf

                            <div class="form-group row">
                                <label for="debt_for"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Naam Schuldige:') }}
                                </label>

                                <div class="col-md-6">
                                    <input id="debt_for" type="text"
                                        class="form-control @error('debt_for') is-invalid @enderror" name="debt_for"
                                        value="{{ old('debt_for', $debt->debt_for) }}" autocomplete="debt_for" autofocus>

                                    @error('debt_for')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">                       
                                <label
                                    class="col-md-4 col-form-label text-md-right mt-4">{{ __('Bedrag:') }}
                                </label>
                                
                                <div class="col-md-3 text-center mb-0">
                                    <label for="euros" >Euro's</label>
                                    <input id="euros" type="text"
                                        class="form-control @error('euros') is-invalid @enderror" name="euros"
                                        value="{{  old('euros', $euros) }}" autocomplete="euros" autofocus>

                                    @error('euros')
                                        <span class="invalid-feedback" role="euros">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-3 text-center">
                                    <label for="cents">Centen</label>
                                    <input id="cents" type="text"
                                        class="form-control @error('cents') is-invalid @enderror" name="cents"
                                        value="{{ old('cents', $cents) }}" autocomplete="cents" autofocus>

                                    @error('cents')
                                        <span class="invalid-feedback" role="cents">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Descriptie') }}
                                </label>

                                <div class="col-md-6">
                                    <textarea id="description" class="form-control @error('description') is-invalid @enderror"
                                        name="description" autocomplete="description"
                                        rows="6">{{ old('description', $debt->description) }}
                                    </textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="status_id"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-6">
                                    <select id="status_id" class="form-control @error('status_id') is-invalid @enderror"
                                        name="status_id" value="{{ old('status_id') }}" required
                                        autocomplete="status_id"
                                        autofocus>
                                        <option value="{{$currentStatus->id}}">{{ $currentStatus->name }}</option>
                                        @foreach($statuses as $status)
                                            <option value="{{$status->id}}">{{ $status->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('status_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Schuld aanpassen') }}
                                    </button>
                                    <a href="javascript:history.back()" style="margin-left: 20px">Terug</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#description'), {
                removePlugins: ['bulletedList', 'numberedList'],
                toolbar: ['Heading', 'bold', 'italic', 'Link', 'blockQuote']
            })
            .then(description => {
                console.log(description);

                const data = description.getData();
                console.log(data);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
