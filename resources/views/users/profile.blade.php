@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header text-center">
                        Profiel van {{ $user->name }}
                    </div>

                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                        aria-orientation="vertical">

                        <div class="col-md-12 col-form-label text-md-center">
                            @if($user->profilepicture == null)
                                <img height="200px" width="200px" src="/img/profile/default/default.png" alt="">
                            @else
                                <img height="200px" width="200px" src="/img/profile/{{ $user->profilepicture }}" alt="">
                            @endif
                        </div>
                    </div>

                    @if($user->profilepicture)
                        <div class="mx-auto">
                            <a href="{{ route('user.destroyProfilePicture', $user) }}"
                                class="btn btn-secondary">Profiel foto verwijderen
                            </a>
                        </div>
                    @endif

                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link text-center" id="v-pills-team-tab" href="{{ route('user.profile', $user) }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false"><b>Mijn profiel</b>
                        </a>
                        <a class="nav-link text-center" id="v-pills-team-tab" href="{{ route('user.myDebts', $user) }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="true">Mijn Schulden
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h1>Mijn profiel</h1>
                    </div>

                    <div class="nav nav-tabs" id="nav-tab" role="tablist" style="margin-bottom: 20px;">
                        <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab"
                           href="#nav-profile" role="tab" aria-controls="nav-profile"
                           aria-selected="true">Profiel
                        </a>

                        <a class="nav-item nav-link" id="nav-password-tab" data-toggle="tab"
                           href="#nav-password" role="tab" aria-controls="nav-password"
                           aria-selected="false">Wachtwoord
                        </a>
                    </div>

                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-profile" role="tabpanel"
                            aria-labelledby="nav-profile-tab">

                            <div class="card-body mt-3">
                                <form method="POST" action="{{ route('user.update', $user) }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">Naam*</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" value="{{ $user->name }}"
                                                class="form-control @error('name') is-invalid @enderror" name="name"
                                                autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">Email*</label>

                                        <div class="col-md-6">
                                            <input id="email" type="text" value="{{ $user->email }}"
                                                class="form-control @error('email') is-invalid @enderror"
                                                name="email"
                                                autocomplete="email" autofocus disabled>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="profile_picture"
                                            class="col-md-4 col-form-label text-md-right">Profiel foto</label>

                                        <div class="col-md-6">
                                            <input type="file" id="profile_picture" name="profile_picture"
                                                class="form-control @error('profile_picture') is-invalid @enderror">

                                            @error('profile_picture')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4 mb-3">
                                            <button type="submit" class="btn btn-secondary">
                                                Gegevens bewerken
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-password" role="tabpanel"
                             aria-labelledby="nav-password-tab">
                            <form method="POST" action="{{ route('user.updatePassword', $user) }}">
                                @method('PUT')
                                @csrf

                                <div class="form-group row">
                                    <label for="password"
                                        class="col-md-4 col-form-label text-md-right">{{ __('Nieuw Wachtwoord') }}
                                    </label>

                                    <div class="col-md-6">
                                        <input id="password" type="password"
                                            class="form-control @error('password') is-invalid @enderror"
                                            name="password" required autocomplete="new-password">

                                            <!-- <p id="passwordHelpBlock" class="form-text text-muted">
                                                Your password must be more than 8 characters long,
                                                should contain at-least 1 Uppercase, 1 Lowercase,
                                                1 Numeric and 1 special character.
                                            </p> -->

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-1">
                                        <img src="/img/app/hint.png" alt="" height="20px" width="20px" 
                                        class="mt-2"
                                        title=
                                        "Jouw wachtwoord moet minimaal uit 8 tekens bestaan met:
1 hoofdletter 
1 kleine letter 
1 nummer 
1 speciale teken">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm"
                                        class="col-md-4 col-form-label text-md-right">{{ __('Wachtwoord bevestigen') }}
                                    </label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control"
                                        name="password_confirmation" required autocomplete="new-password">
                                    </div>

                                    <div class="col-md-1">
                                        <img src="/img/app/hint.png" alt="" height="20px" width="20px" 
                                        class="mt-2" title="Confirm password">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4" style="margin-bottom: 20px;">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Wachtwoord bewerken') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
