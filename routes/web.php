<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// Admin
Route::get('/admin/users', 'AdminController@users')->name('admin.users');
Route::get('/admin/debts', 'AdminController@debts')->name('admin.debts');
Route::get('/admin/statuses', 'AdminController@statuses')->name('admin.statuses');
Route::get('/admin/roles', 'AdminController@roles')->name('admin.roles');

// Users
Route::get('/user/index', 'UserController@index')->name('user.index');
Route::get('/user/create', 'UserController@create')->name('user.create');
Route::post('/user/store', 'UserController@store')->name('user.store');
Route::get('/user/{user}/show', 'UserController@show')->name('user.show');
Route::get('/user/{user}/edit', 'UserController@edit')->name('user.edit');
Route::put('/user/{user}/update', 'UserController@update')->name('user.update');
Route::delete('/user/{user}/delete', 'UserController@destroy')->name('user.destroy');
Route::get('/user/{user}/profile', 'UserController@profile')->name('user.profile');
Route::get('/user/{user}/myDebts', 'UserController@myDebts')->name('user.myDebts');
Route::put('/user/{user}/updatePassword', 'UserController@updatePassword')->name('user.updatePassword');
Route::put('/user/{user}/updateProfilePicture', 'UserController@updateProfilePicture')->name('user.updateProfilePicture');
Route::get('/user/{user}/destroyProfilePicture', 'UserController@destroyProfilePicture')->name('user.destroyProfilePicture');

// Debts
Route::get('/debt/index', 'DebtController@index')->name('debt.index');
Route::get('/debt/create', 'DebtController@create')->name('debt.create');
Route::post('/debt/store', 'DebtController@store')->name('debt.store');
Route::get('/debt/{debt}/show', 'DebtController@show')->name('debt.show');
Route::get('/debt/{debt}/edit', 'DebtController@edit')->name('debt.edit');
Route::put('/debt/{debt}/update', 'DebtController@update')->name('debt.update');
Route::delete('debt/{debt}/delete', 'DebtController@destroy')->name('debt.destroy');

// Roles
Route::get('/role/index', 'RoleController@index')->name('role.index');
Route::get('/role/create', 'RoleController@create')->name('role.create');
Route::post('/role/store', 'RoleController@store')->name('role.store');
Route::get('/role/{role}/show', 'RoleController@show')->name('role.show');
Route::get('/role/{role}/edit', 'RoleController@edit')->name('role.edit');
Route::put('/role/{role}/update', 'RoleController@update')->name('role.update');
Route::delete('role/{role}/delete', 'RoleController@destroy')->name('role.destroy');

// Statuses
Route::get('/status/index', 'StatusController@index')->name('status.index');
Route::get('/status/create', 'StatusController@create')->name('status.create');
Route::post('/status/store', 'StatusController@store')->name('status.store');
Route::get('/status/{status}/show', 'StatusController@show')->name('status.show');
Route::get('/status/{status}/edit', 'StatusController@edit')->name('status.edit');
Route::put('/statuse/{status}/update', 'StatusController@update')->name('status.update');
Route::delete('status/{status}/delete', 'StatusController@destroy')->name('status.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
