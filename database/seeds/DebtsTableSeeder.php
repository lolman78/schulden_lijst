<?php

use Illuminate\Database\Seeder;

class DebtsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('debts')->insert([
            [
                'debt_for' => 'Jeff',
                'price' => '20.56',
                // 'euros' => '20',
                // 'cents' => '50',
                'status_id' => '1',
                'user_id' => '1',
                'created_at' => now()
            ],
            [
                'debt_for' => 'Mark',
                'price' => '00.99',
                // 'euros' => '00',
                // 'cents' => '50',
                'status_id' => '1',
                'user_id' => '1',
                'created_at' => now()
            ],
            [
                'debt_for' => 'Lucina',
                'price' => '50000.78',
                // 'euros' => '5000',
                // 'cents' => '00',
                'status_id' => '1',
                'user_id' => '3',
                'created_at' => now()
            ],
            [
                'debt_for' => 'Steve',
                'price' => '690.00',
                // 'euros' => '690',
                // 'cents' => '00',
                'status_id' => '1',
                'user_id' => '2',
                'created_at' => now()
            ],
            [
                'debt_for' => 'marco',
                'price' => '7050.05',
                // 'euros' => '7050',
                // 'cents' => '50',
                'status_id' => '1',
                'user_id' => '1',
                'created_at' => now()
            ],
            [
                'debt_for' => 'Ali',
                'price' => '1000000.50',
                // 'euros' => '1000000',
                // 'cents' => '50',
                'status_id' => '1',
                'user_id' => '1',
                'created_at' => now()
            ],
        ]);
    }
}
