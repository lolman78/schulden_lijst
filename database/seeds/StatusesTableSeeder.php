<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [
                'name' => 'Niet Betaalt',
                'created_at' => now()
            ],
            [
                'name' => 'Betaalt',
                'created_at' => now()
            ],
            [
                'name' => 'Test',
                'created_at' => now()
            ],
        ]);
    }
}
