<?php

namespace App\Policies;

use App\User;
use App\Debt;
use Illuminate\Auth\Access\HandlesAuthorization;

class DebtPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createAndStore(User $user)
    {
      // Als de role van de user Gebruiker of Admin is, mogen ze een schuld maken
      return ($user->role->name == 'Gebruiker') || ($user->role->name == 'Admin');
    }

    public function show(User $user, Debt $debt)
    {
      // Als de schuld van de ingelogde user is of de role van de user "admin" is, mogen ze de schuld bekijken.
      return ($debt->user_id == $user->id) || ($user->role->name == 'Admin');
    }
 
    public function editAndUpdate(User $user, Debt $debt)
    {
      // Als de schuld van de ingelogde user is of de role van de user "admin" is, mogen ze de schuld aanpassen.
      return ($debt->user_id == $user->id) || ($user->role->name == 'Admin');
    }

    public function destroy(User $user, Debt $debt)
    {
      // Als de schuld van de ingelogde user is of de role van de user "admin" is, mogen ze de schuld verwijderen.
      return ($debt->user_id == $user->id) || ($user->role->name == 'Admin');
    }
}
