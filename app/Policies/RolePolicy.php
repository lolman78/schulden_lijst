<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function isAdmin(User $user)
    {
        // Als de role van de user: admin is mogen ze een role maken.
        return $user->role->name == 'Admin';
    }

    public function createAndStore(User $user)
    {
        // Als de role van de user: admin is mogen ze een role maken.
        return $this->isAdmin($user);
    }

    public function editAndUpdate(User $user)
    {
        // Als de role van de user: admin is mogen ze de role bewerken.
        return $this->isAdmin($user);
    }

    public function destroy(User $user)
    {
        // Als de role van de user: admin is mogen ze de role verwijderen.
        return $this->isAdmin($user);
    }
}
