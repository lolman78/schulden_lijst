<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Debt;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UpdateUserPasswordRequest;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('createAndStore', User::class);
        $roles = Role::orderBy('created_at')->get();

        return view('users.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $this->authorize('createAndStore', User::class);

        // Als de gebruiker is gevalideerd maak de user aan
        $user = User::create([
            'name' => request()->name,
            'email' => request()->email,
            'role_id' => 2,
            'password' => Hash::make(request()->password),
        ]);

        $user->save();

        return redirect()->route('admin.users')->with('success', 'Gebruiker aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // we hebben al de profile page en de funtie die je daar heen brengt dus dit is best wel puntloos
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('editAndUpdate', $user);
        $roles = Role::orderBy('created_at')->whereNotIn('id', [$user->role_id])->get();

        return view('users.edit', ['roles' => $roles, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('editAndUpdate', $user);
        $user->update(request()->all());

        // Kijk of er een foto in de request zit die gekoppeld moet worden aan de user.
        // Zo ja sla het plaatje op en koppel het plaatje aan de user.
        if($request->hasFile('profile_picture'))
        {
            // Haal het oude plaatje eruit voordat je de nieuwe erin zet.
            if(file_exists(public_path('img/profile/'.$user->profilepicture)))
            {
                // Als de image die eraan gekoppeld de default is hoef je de image niet te verwijderen
                if($user->profilepicture !== null)
                {
                    unlink(public_path('img/profile/'.$user->profilepicture));
                }

                $extension = $request->profile_picture->extension();
                $imageName = Str::snake($user->name) . '.' . $extension; 
                // met $imageName = Str::snake($user->name) . '.' . $extension;
                // krijg je hetzelfde maar met alleen kleine letters

                request()->profile_picture->move(public_path('img/profile/'), $imageName);
                $user->profilepicture = $imageName;

                $user->save();

            }else{

                dd('File does not exists.');

            }
        }
        else // als de user de naam heeft aangepast en niet een nieuwe profielfoto heeft geupload voer deze code uit
        {
            // als de profiel foto default is hoef je lokaal niet aan te passen
            if($user->profilepicture !== null)
            {
                // Haal de oude profielfotonaam naam op
                $oldProfilepictureName = public_path('img/profile/'.$user->profilepicture);

                // Pak de nieuwe profiel foto naam
                $filename = $user->profilepicture;
                $extension = pathinfo( $filename, PATHINFO_EXTENSION );
                $newProfilepictureName = public_path('img/profile/'.$user->name.'.'.$extension);

                // Verander de naam lokaal
                rename($oldProfilepictureName, $newProfilepictureName);

                // Verander de profilepicture naam in de database
                $user->profilepicture = $user->name.'.'.$extension;
                $user->save();
            }
        }

        return back()->with('success', 'Gebruiker bewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('destroy', $user);

        // Haal alle debts op van de user die verwijderd gaat worden
        $userDebts = Debt::orderBy('created_at')->where('user_id', '=', $user->id)->get();

        // Verwijder alle debts die horen bij de user die je gaat verwijderen
        foreach($userDebts as $userDebt)
        {
            $userDebt->delete();
        }

        // Als de image die eraan gekoppeld de default is hoef je de image niet te verwijderen
        if($user->profilepicture !== null)
        {
            unlink(public_path('img/profile/'.$user->profilepicture));
        }

        $user->delete();

        return back()->with('success', 'Gebruiker verwijderd');
    }

    public function myDebts(User $user)
    {
        // authorization and getting debts for myDebts page
        $this->authorize('Profile', $user);
        $debts = Debt::orderBy('created_at')->where('user_id', '=', $user->id)->get();
        
        // send the user to the myDebts page(myDebts.blade.php)
        return view('users.myDebts', ['user' => $user, 'debts' => $debts]);
    }

    public function profile(User $user)
    {
        // authorization and and send the user the profile page
        // maak het in de toekomst zo dat hij beide functies in 1 functie doet
        $this->authorize('Profile', $user);

        return view('users.profile', ['user' => $user]);
    }

    public function destroyProfilePicture(User $user)
    {
        $this->authorize('Profile', $user);

        // remove the profile picture from the database and localy
        // Als de image die eraan gekoppeld de default is hoef je de image niet te verwijderen
        if($user->profilepicture !== null)
        {
            unlink(public_path('img/profile/'.$user->profilepicture));
        }

        $user->profilepicture = null;
        $user->update();

        // return to the myDebts page
        return back()->with('success', 'Profiel foto verwijderd');
    }

    // voor later
    public function updatePassword(UpdateUserPasswordRequest $request, User $user)
    {
        $this->authorize('updatePassword', $user);
        
        request()->merge(
            [
                'password' => bcrypt(request()->password),
            ]);

        $user->update(request()->all());

        return back()->with('success', 'Je wachtwoord is bewerkt!');
    }
}
