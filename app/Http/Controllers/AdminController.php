<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Debt;
use App\Role;
use App\Status;

class AdminController extends Controller
{
    public function users()
    {
        $this->authorize('isAdmin', User::class);
        $users = User::orderBy('name')->get();

        return view('admin.users', compact('users'));
    }

    public function debts()
    {
        $this->authorize('isAdmin', User::class);
        $debts = Debt::orderBy('created_at')->get();

        return view('admin.debts', compact('debts'));
    }

    public function statuses()
    {
        $this->authorize('isAdmin', User::class);
        $statuses = Status::orderBy('name')->get();

        return view('admin.statuses', compact('statuses'));
    }

    public function roles()
    {
        $this->authorize('isAdmin', User::class);
        $roles = Role::orderBy('name')->get();

        return view('admin.roles', compact('roles'));
    }
}
