<?php

namespace App\Http\Controllers;

use App\User;
use App\Debt;
use App\Status;
use Illuminate\Http\Request;
use App\Http\Requests\StoreDebtRequest;
use App\Http\Requests\UpdateDebtRequest;

class DebtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('createAndStore', Debt::class);

        return view('debts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDebtRequest $request)
    {
       $this->authorize('createAndStore', Debt::class);

       // Als de Schuld is gevalideerd maak de Schuld aan
       $user = Debt::create([
           'debt_for' => request()->debt_for,
           'description' => request()->description,
           'price' => request()->euros.'.'.request()->cents,
           'status_id' => request()->status_id,
           'user_id' => request()->user_id,
       ]);
       
       $debt = Debt::latest()->first();
       
       return redirect()->route('debt.show', compact('debt'))->with('success', 'Nieuwe schuld aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Debt  $debt
     * @return \Illuminate\Http\Response
     */
    public function show(Debt $debt)
    {
        $this->authorize('show', $debt);

        return view('debts.show', compact('debt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Debt  $debt
     * @return \Illuminate\Http\Response
     */
    public function edit(Debt $debt)
    {
        $this->authorize('editAndUpdate', $debt);

        $euros = intval($debt->price);

        // $price = $debt->price;
        // $whole = intval($price); // 1234
        // $decimal1 = $price - $whole; // 0.44000000000005 uh oh! that's why it needs... (see next line)
        // $decimal2 = round($decimal1, 2); // 0.44 this will round off the excess numbers
        // $cents = substr($decimal2, 2); // 44 this removed the first 2 characters

        $whole = floor($debt->price);
        $cents = ($debt->price - $whole) * 100;
        $cents = round($cents);

        $statuses = Status::orderBy('name')->whereNotIn('id', [$user->status_id])->get();

        return view('debts.edit', compact('debt', 'statuses', 'euros', 'cents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Debt  $debt
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDebtRequest $request, Debt $debt)
    {
        $this->authorize('editAndUpdate', $debt);

        $debt->price = $request->euros.'.'.$request->cents;

        $debt->update(request()->all());

        // verander dit in de toekomst naar debts.show
        return redirect()->route('debt.show', compact('debt'))->with('success', 'schuld is succesvol bijgewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Debt  $debt
     * @return \Illuminate\Http\Response
     */
    public function destroy(Debt $debt)
    {
        $this->authorize('destroy', $debt);

        $debt->delete();
        $user = User::findOrFail($debt->user_id);

        return redirect()->route('user.myDebts', compact('user'))->with('success', 'Schuld verwijderd');
    }
}
