<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Debt;
use App\Role;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(Auth::User() === null){

            return view('home');
        }

        $user = User::findOrFail(Auth::User()->id);
        $debts = Debt::orderBy('created_at')->where('User_id', '=', $user->id)->get();
          
        $numberOfDebts = count($debts);
        $unpaidDebts = Debt::orderBy('created_at')->where('User_id', '=', $user->id)->where('Status_id', '=', 1)->get();
        $paidDebts = Debt::orderBy('created_at')->where('User_id', '=', $user->id)->where('Status_id', '=', 2)->get();
        $unrecievedMoney = 0;
        $recievedMoney = 0;

        // al het geld van zijn schulden die betaalt zijn
        foreach($paidDebts as $paidDebt)
        {
            $recievedMoney += $paidDebt->price;
        }

        // al het geld van zijn schulden die nog niet betaalt zijn
        foreach($unpaidDebts as $unpaidDebt)
        {
            $unrecievedMoney += $unpaidDebt->price;
        }

        $unpaidDebts = count($unpaidDebts);
        $paidDebts = count($paidDebts);

        return view('home', compact('user', 'recievedMoney', 'unrecievedMoney', 'paidDebts', 'unpaidDebts', 'numberOfDebts'));
    }
}
