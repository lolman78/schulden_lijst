<?php

namespace App\Http\Requests;

use Auth;
use App\Debt;
use App\Status;
use Illuminate\Foundation\Http\FormRequest;

class UpdateDebtRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        // haal het idee van de ingelogde user op
        $user_id = Auth::User()->id;

        // zet de description in een variabele
        $description = Request()->get("description");

        // haal alle <p> tags eruit
        $description = str_replace("<p>", "", $description);

        // verander de </p> tags met <br>
        $description = str_replace("</p>", "<br>", $description);

        request()->merge(
        [
            'user_id' => $user_id,
            'description' => $description,
        ]);

        // als de user de centen invul veld leeg heeft gelaten maak het automatisch 0 centen
        if(request()->cents === null)
        {
            request()->merge(
            [
                'cents' => "00",
            ]);  
        }

        if(strlen(request()->cents) === 1)
        {
            request()->merge(
            [
                'cents' => '0'.request()->cents,
            ]);   
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            request()->validate([
                'debt_for' => 'required|max:255',
                'description' => 'nullable|string|max:1000',
                'euros' => 'required|string|min:1|max:19|regex:/[0-9]{1,}/',
                'cents' => 'string|min:1|max:2|regex:/[0-9]{1,2}/',
                'status_id' => 'required',
                'user_id' => 'required',
            ])
        ];
    }
}
