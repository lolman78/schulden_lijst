<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['name'];

    public function debts()
    {
        return $this->hasMany(Debt::class);
    }
}